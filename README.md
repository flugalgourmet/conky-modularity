# Conky Modularity

My collection of conkys and bash scripts to beautify your desktop and to monitor various systems. Modular in design, put it together like a puzzle or use my predefined configuration.

+ autostart: Contains the file that will launch the conkys at startup. Call this in your autostart configuration. Be aware, order matters. To maintain proper alignment, some conkys may need to exist before the others will line up.
+ backgrounds: Conkys' that provide a background for the other conkys'. Should always be loaded first.
+ krells: This defines the gkrellm replacement. I did this because my hard drives were skipping devices sometimes. I never though it would turn out this good though.
+ launchers: This is the main launcher for conky. You should never touch this.
+ main: These define the main conkys which take up the most space on the desktop.
+ monitoring: These are the predefined conkys which you call from the autostart file.
+ scripts: Scripts that are required by the conkys.

+ Bugs:
	+ SOLVED! Clicking on the desktop portion where there is no conky brings the background forward.

+ Conky types:
	+ backgrounds:
		+ translucent: 150% black which allows you to see your photos but still see the conkys'.
	+ krells:
		+ cpu usage graphs
		+ drive usage graphs
		+ internet usage graphs
	+ main:
		+ internet connections (tcp and udp only)
		+ system:
			+ uptime
			+ temperature
			+ processes (total)
			+ load (total)
			+ local ip
			+ public ip
			+ ram bar
		+ drive free space
		+ drive temperature
		+ cpu cores:
			+ frequency
			+ temperature
		+ cpu processes
		+ memory processes
		+ network uploads and downloads:
			+ yesterday
			+ today
			+ month
		+ nvidia gpu:
			+ temperature
			+ memory used
			+ memory free
			+ power draw
			+ fan speed
